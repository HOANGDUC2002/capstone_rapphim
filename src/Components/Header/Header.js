import React from "react";
import { NavLink } from "react-router-dom";
import UseNav from "./UseNav";

export default function Header() {
  return (
    <div className="px-20 py-10 flex justify-between items-center shadow-lg ">
      <NavLink to={"/"}>
        <span className="text-red-600 text-3xl font-medium"> CyberFlix</span>
      </NavLink>
      <UseNav />
    </div>
  );
}
