import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "../Service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constans/userConstan";
import { userLocalService } from "../Service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/87898-christmas-wind-chimes.json";
import {
  setUserAction,
  setUserActionService,
} from "../../redux/action/userAction";
export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log("res: ", res);
        message.success("Đăng nhập thành công");
        dispatch(setUserAction(res.data.content));
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
          // window.location.href = "/";(dùng cái này chuyển trng sẽ mất hết dữ liệu đc lưu)
        }, 1000);
      })
      .catch((err) => {
        console.log("err: ", err);
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishReduxThunk = (values) => {
    const handleNavigate = () => {
      setTimeout(() => {
        navigate("/");
        // window.location.href = "/";(dùng cái này chuyển trng sẽ mất hết dữ liệu đc lưu)
      }, 1000);
    };
    dispatch(setUserActionService(values, handleNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex justify-center items-center bg-blue-500">
      <div className="container p-5 rounded bg-white flex ">
        <div className="w-1/2 ">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2"></div>
        <div className="w-1/2">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
            >
              <Button
                className="bg-blue-500 hover:text-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
