import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default function Detail() {
  let params = useParams();
  console.log("params: ", params);
  return (
    <div>
      <h2 className="text-red-500 text-center font-black">
        Mã phim: {params.id}
      </h2>
    </div>
  );
}
